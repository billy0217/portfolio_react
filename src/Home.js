import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Header from './Header';
import Footer from './Footer';

class Home extends React.Component {

  getDefaultProps() {
    return {
      // allow the initial position to be passed in as a prop
      initialPos: {x: 0, y: 0}
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      x: 0,
      y: 0
    };
  }

  getInitialState() {
    return {
      pos: this.props.initialPos,
      dragging: false,
      rel: null // position relative to the cursor
    }
  }

  componentDidMount() {
    const bkImg = document.querySelector('.home');
    let targetX = bkImg.clientWidth;
    let targetY = bkImg.clientHeight;
     this.setState({
        x: targetX,
        y: targetY
     });

     this.onMove;
  }

  componentDidUpdate(props, state) {
    document.addEventListener('mousemove', this.onMove)
  }

  onMove(e){
    e.preventDefault();
    const bkImg = document.querySelector('.home');
    const walk = 50;
    if(bkImg){
      

      let targetX = bkImg.clientWidth;
      let targetY = bkImg.clientHeight;
     
      let pageX = e.pageX - (targetX / 2);
      let pageY = e.pageY - (targetY / 2);
      
      let xWalk = Math.round((walk / targetX) * pageX * -1 -25);
      let yWalk = Math.round((walk / targetY) * pageY * -1 -30); 
      
      bkImg.style.backgroundPosition = `${xWalk}px ${yWalk}px`;
      
    }
    e.stopPropagation()
    e.preventDefault()
  }

  render() {
    return (
      <div className="landing-page">
        <Header />
        <section onMouseMove={this.onMove} className="home" id="home">
          <div className="content">
            <ReactCSSTransitionGroup 
                component="div"
                transitionName="slideUp"
                transitionEnterTimeout={500}
                transitionLeaveTimeout={500}
                transitionAppear={true}
                transitionAppearTimeout={500}
            >
              <h2>Hi, I'm<br/>Front End<br/>Web Developer <span className="blink"></span></h2>
            </ReactCSSTransitionGroup>  
          </div>    
        </section>
        <Footer />
      </div>
    );
  }
}



export default Home;
