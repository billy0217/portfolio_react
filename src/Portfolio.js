import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Header from './Header';
import Footer from './Footer';
import Projects from './Projects';

class Portfolio extends React.Component {

  render() {
    return (
      <div className="internal">
        <Header />
        <section className="portfolio" id="portfolio">
          <div className="container">
            <ReactCSSTransitionGroup 
                component="div"
                transitionName="slideUp"
                transitionEnterTimeout={500}
                transitionLeaveTimeout={500}
                transitionAppear={true}
                transitionAppearTimeout={500}
            >
              <div className="text-center">
                  <h2 className="underline">{this.props.title}</h2>
              </div>
              <div className="project_wrapper">
                <Projects />
              </div>

            </ReactCSSTransitionGroup>  
          </div>    
        </section>
        <Footer />
      </div>
    );
  }
}



export default Portfolio;
