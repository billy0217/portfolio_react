import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {ProjectList} from './data';

class Projects extends Component {


  handleClick(e) {
    e.preventDefault();
    console.log('this is:', this);
    console.log(e.currentTarget);
    let project = e.currentTarget;

    let projectWrapper = project.parentNode.parentNode;

    projectWrapper.classList.add('open');

  }

  handleClose(e){
    let closeBtn = e.currentTarget;
    let parent = closeBtn.parentNode.parentNode.parentNode;
    parent.classList.remove('open');
  }

  render() { 

    let projects = ProjectList.map((project)=>{
      return (
        <div className="project" key={project.id}>
          <div className="project_thumb" key={project.id} >
            <img src={project.thumb} alt={ 'project_'+project.name} />
            <div onClick={(e) => this.handleClick(e)} className="content_short_info">
              <h4>{project.name}</h4>
              <p>{project.workfrom}</p>
              <p>{project.type}</p>
            </div>
          </div>
          <div className="project_info">
            <div className="content">
              <div onClick={this.handleClose} className="close"></div>
              <div className="clearfix">
                <div className="col-12">
                  <div className="img_wrapper">
                    <img src={project.img} alt={ 'project_'+project.name} />
                  </div>
                </div>
                <div className="col-12">  
                  <h4>{project.name}</h4>
                  <p className="text-blue"><small>{project.workfrom}</small></p>
                  <p>{project.description}</p>
                  <p><strong>Technologies :</strong> {project.tech}</p>
                  <p><a className="btn" href={project.link} target="_blank">{project.type === 'Web site' ? 'Visit web site' : 'View in App Store'}</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    });

    return(
        <div className="project_container">
          {projects}
        </div>
    );
  }
}

export default Projects;
