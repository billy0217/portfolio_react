import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class Footer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {date: new Date()};
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  render() {
    return (
      <footer className="footer">
        <div className="footer_wrapper">
         <p>&copy; {this.state.date.getFullYear()} Billy Han. All Rights Reserved.</p>    
        </div>
      </footer>
    );
  }
}



export default Footer;
