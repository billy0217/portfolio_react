import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
//import '../css/style.css';
 
// Filterable CheatSheet Component
ReactDOM.render(
	<App />, 
	document.getElementById('container')
);