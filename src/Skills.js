import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {SkillsSet} from './data';



const Skills = () => {


    let skillls = SkillsSet.map((skillset) => {
	    return (
	      	<div className="skillset" key={skillset.id} >
	        	<h4>{skillset.skill}</h4>
	        	<div className="progress-bar" data-percent={skillset.level/5}></div>
	      	</div>
	    );
    }); 
  
  return (
      <div className="group">
        {skillls}    
      </div>
  );
}

export default Skills;