import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Header extends React.Component {
  
  componentDidMount() {
    const container = document.querySelector('#container');
    const navWrapper = document.querySelector('.main_nav .nav');
    const navItems = navWrapper.querySelectorAll('a');
    
    function clickHandle(){
      if(container.classList.contains('nav_open')){
        container.classList.remove('nav_open')
      }
    }    

    if(navItems){
      navItems.forEach((navItem)=>{
        //console.log(navItem);
        navItem.addEventListener("click", clickHandle);
      });
    }

  }

  navToggle(){
    const container = document.querySelector('#container');
    container.classList.toggle('nav_open');
  }

  render() {

    return (
      <header>
        <div className="header_wrapper">
          <div className="logo_wrapper">
           <NavLink exact to="/"><img src="img/logo.png" alt="" /></NavLink>
          </div>
          <div className="main_nav">
            <ul className="nav">
              <li><NavLink exact to="/"><i className="fa fa-home"></i></NavLink></li>
              <li><NavLink to="/about" activeClassName="active">About</NavLink></li>
              <li><NavLink to="/work" activeClassName="active">Work</NavLink></li>
              <li><NavLink to="/contact" activeClassName="active">Contact</NavLink></li>
            </ul>
          </div>
          <div onClick={this.navToggle} className="hamburger_nav"><div className="bars"></div></div>
        </div> 
      </header>
    );
  }
}

export default Header;