import React from 'react';
import { NavLink } from 'react-router-dom';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Header from './Header';
import Footer from './Footer';
import Skills from './Skills';

class About extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentDate: new Date()
    };
  }

  dateDifference(){
    let df= new Date("10/14/2013");
    let dt = new Date();   
    let allMonths= dt.getMonth() - df.getMonth() + (12 * (dt.getFullYear() - df.getFullYear()));
    let allYears= dt.getFullYear() - df.getFullYear();
    let partialMonths = dt.getMonth() - df.getMonth();
    let months;
    if (partialMonths < 0) {
        allYears--;
        partialMonths = partialMonths + 12;
    }

    if( 0 < allYears ){
      allYears += ' years';
    }

    if( 1 <= partialMonths){
      months = " and " + partialMonths + " months"
    }else{
      months = "";
    }

    let total = allYears + months;
    
    return total;
  }

  skillsSet(){
    console.log(SkillsSet);
  }

  render() {
    return (
      <div className={this.props.page}>
        <Header />
        <div className="top_banner">

        </div>
        <section className="about" id="about">
          <div className="container">
            <ReactCSSTransitionGroup 
                component="div"
                transitionName="slideUp"
                transitionEnterTimeout={500}
                transitionLeaveTimeout={500}
                transitionAppear={true}
                transitionAppearTimeout={500}
            >
              <div className="col-12 text-center">
                <h2 className="underline">{this.props.title}</h2>
              </div>

              <div className="clearfix">
                <div className="left-col col-8 offset-2 text-center">
                  <p>I am an Auckland based Web Developer.</p>
                  <p>I am predominantly a Front End Developer with more than {this.dateDifference()} of professional experience, who's passionate about working with responsive and interactive websites</p>
                  <p>I'm a very responsible and hard-working professional. I really like being a developer and enjoy learning the a latest technologies are helpful for the company and its customers.</p>
                  <p>If you want to know more about me? <strong><NavLink to="/contact">Get in touch !!</NavLink></strong></p>
                  <p>Check out the resume to see the highlights.</p>
                  <p><a className="download_link" target="_blank" href="cv.pdf"><i className="fa fa-file-text-o"></i> <span>Download<br/>CV</span></a></p>
                </div>
              </div>
              {/*
              <div className="col-12 text-center">
                <h3 className="underline">Skills</h3>
              </div>
              <div>
                <Skills />
              </div>
            */}
            </ReactCSSTransitionGroup>
          </div> 
        </section>
        <Footer />
      </div>
    )
  }  
}

export default About;