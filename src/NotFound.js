import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Header from './Header';
import Footer from './Footer';

const NotFound = () => (
	<div className="internal">
        <Header />
        <section className="notfound" id="notfound">
          <div className="container">
            <ReactCSSTransitionGroup 
                component="div"
                transitionName="slideUp"
                transitionEnterTimeout={500}
                transitionLeaveTimeout={500}
                transitionAppear={true}
                transitionAppearTimeout={500}
            >
            	<div className="text-center">
    				<h2>Page Not Found</h2>
    			</div>

            </ReactCSSTransitionGroup>  
          </div>    
        </section>
        <Footer />
    </div>
);

export default NotFound;