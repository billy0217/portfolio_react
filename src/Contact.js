import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Header from './Header';
import Footer from './Footer';

class Contact extends React.Component {

  render() {
    return (
      <div className="internal">
        <Header />
        <section className="contact" id="contact">
          <div className="container">
            <ReactCSSTransitionGroup 
                component="div"
                transitionName="slideUp"
                transitionEnterTimeout={500}
                transitionLeaveTimeout={500}
                transitionAppear={true}
                transitionAppearTimeout={500}
            >
              <div className="text-center">
                  <h2 className="underline">{this.props.title}</h2>
                  <p>Wanna get in touch or talk to me about a project?</p>
                  <p>Just flick me an email to <a href="mailto:nada_billy0217@hotmail.com">nada_billy0217@hotmail.com</a> and ~let's talk.</p>
              </div>

            </ReactCSSTransitionGroup>  
          </div>    
        </section>
        <Footer />
      </div>
    );
  }
}



export default Contact;
