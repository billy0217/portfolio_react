import React from 'react';
import {
	BrowserRouter,
	Route,
	Switch,
	browserHistory
} from 'react-router-dom';
import SearchableTable from './serach';
import Home from './Home';
import Footer from './Footer';
import About from './About';
import Portfolio from './Portfolio';
import PageTemplate from './PageTemplate';
import Contact from './Contact';
import NotFound from './NotFound';
import {data} from './data';



const App = ({match}) => (
	
	<BrowserRouter>
		<div className="main_wrapper">
			<Switch>
				<Route exact path="/" component={Home} />
				<Route path="/about" render={() => <About title='About' page='internal' /> } />
				<Route path="/work" render={() => <Portfolio title='Work' page='internal' /> } />
				<Route path="/contact" render={() => <Contact title='Get in touch' page='internal' /> } />
				<Route path="*" component={NotFound} />
			</Switch>
		</div>
	</BrowserRouter>
);

export default App;