export const SkillsSet = [
	{
		id : '1',
		skill : 'HTML',
		level : '95'
	},
	{
		id : '2',
		skill : 'CSS3',
		level : '95'
	},
	{
		id : '3',
		skill : 'Bootstrap',
		level : '90'
	},
	{
		id : '4',
		skill : 'Task runner (gulp)',
		level : '70'
	},
	{
		id : '5',
		skill : 'SASS',
		level : '80'
	},
	{
		id : '6',
		skill : 'LESS',
		level : '80'
	},
	{
		id : '7',
		skill : 'Jquery',
		level : '85'
	},
	{
		id : '8',
		skill : 'JavaScript',
		level : '75'
	},
	{
		id : '9',
		skill : 'PHP',
		level : '80'
	},
	{
		id : '10',
		skill : 'MySQL',
		level : '75'
	},
	{
		id : '11',
		skill : 'WordPress',
		level : '80'
	},
	{
		id : '12',
		skill : 'Concrete5',
		level : '80'
	},
	{
		id : '13',
		skill : 'Adobe Suite',
		level : '85'
	},
	{
		id : '14',
		skill : 'GIT',
		level : '70'
	}
];

export const ProjectList = [
	{
		id: '1',
		name : 'Spark Arena',
		link : 'https://www.sparkarena.co.nz/',
		img : 'img/sparkarena-img.jpg',
		thumb : 'img/sparkarena-thumb.jpg',
		description : 'Spark Arena (formerly Vector Arena) named for sponsor Spark New Zealand. So Spark Arena wanted to apply new Spark Arena brand designs to existing Vector Arena website with adaptive Mobile responsive design. I worked in a full-stack development role and Teamed up with a desinger and Backend Developer to buile website that simple to use and presents the informantion in a user-friendly format.',
		client : 'Spark Arena',
		workfrom : 'From Tin Solider',
		tech : 'HTML5, CSS3, jQuery, JavaScript, Adobe ColdFusion',
		role : 'Full Stack Developer',
		type : 'Web site'
	},
	{
		id: '2',
		name : 'Tin Soldier',
		link : 'https://tinsoldier.co.nz/',
		img : 'img/tinsoldier-img.jpg',
		thumb : 'img/tinsoldier-thumb.jpg',
		description : 'Tin Soldier wanted a new re branded  website with that had a simple and trandy design withCMS so we could easily manage and edit content and showcase projects. I worked in a full-stack development role, with a designer and build responsive website using Concrete 5 CMS.',
		client : '',
		workfrom : 'From Tin Soldier',
		tech : 'HTML5, CSS3, LESS, jQuery, JavaScript, PHP, MySQL',
		role : 'Full Stack Developer',
		type : 'Web site'
	},
	{
		id: '3',
		name : 'Aunt Betty’s',
		link : 'http://auntbettys.co.nz/',
		img : 'img/auntbettys-img.jpg',
		thumb : 'img/auntbettys-thumb.jpg',
		description : 'Aunt Betty’s wanted to a new responsive CMS webite that had a interative and competition form integrated with Facebook page. I was teamed up with a Backend Developer, and build a responsive website with CSS3 animation based on Concrete5 CMS and integrated competition module to with the Facebook App.',
		client : '',
		workfrom : 'From Tin Soldier',
		tech : 'HTML5, CSS3, jQuery, JavaScript, PHP, MySQL',
		role : 'Web Developer',
		type : 'Web site'
	},
	{
		id: '4',
		name : 'Clever Made Easy',
		link : 'http://www.clevermadeeasy.co.nz/',
		img : 'img/clevermadeeasy-img.jpg',
		thumb : 'img/clevermadeeasy-thumb.jpg',
		description : 'Clever, made easy was the catch-phrase for a new Panasonic integrated marketing promotion. Client wanted to build new website with that had a Recipe, Product and Promtions modules, which allow easy management of recipes and product offerings and runing promtions.I worked in a full-stack development role with desiger and working closely with their design agency. The site build on Concrete 5 CMS and includes a one-page interactive product introduction, built with Javascript and CSS3 effects',
		client : '',
		workfrom : 'From Tin Soldier',
		tech : 'HTML5, SCSS, Gulp, ScrollMagic.JS, jQuery, JavaScript, PHP, MySQL',
		role : 'Full Stack Developer',
		type : 'Web site'
	},
	{
		id: '5',
		name : 'Saint Kentigern',
		link : 'http://www.saintkentigern.com/',
		img : 'img/saintkentigern-img.jpg',
		thumb : 'img/saintkentigern-thumb.jpg',
		description : 'Satint Kentigern School wanted a new responsive CMS webite that had a mulity online application forms and Donation form that integration with Payment Gateways. I worked in a full-stack development role, with a designer and third-party designer base on Concretr 5 CMS.',
		client : '',
		workfrom : 'From Tin Soldier',
		tech : 'HTML5, CSS3,  jQuery, JavaScript, PHP, MySQL',
		role : 'Full Stack Developer',
		type : 'Web site'
	},
	{
		id: '6',
		name : 'Covertax Ltd',
		link : 'http://www.covertex.co.nz/',
		img : 'img/covertex-img.jpg',
		thumb : 'img/covertex-thumb.jpg',
		description : 'Covertex wanted to reskin existing WordPress website that had Blog, Latest News and Video functions to improve their SEO. I worked in full stack web developer to maintain supporting website',
		client : 'COVERTAX Ltd',
		workfrom : 'Freelance work',
		tech : 'HTML5, CSS3, jQuery, JavaScript, PHP, MySQL',
		role : 'Web Developer',
		type : 'Web site'
	},
	{
		id: '7',
		name : 'Coconut Mobile Ltd',
		link : 'https://play.google.com/store/apps/details?id=com.coconuthq.coconutapp&hl=en',
		img : 'img/coconut-app-img.jpg',
		thumb : 'img/coconut-app-icon.jpg',
		description : 'Coconut Mobile Ltd wanted build an advertising platform app that rewards its users everytime they watch advertisement. Client had prive concepts of app, so I was helping to improve branding and UX/UI design of the app. I also build front end of app with HTML5 CSS3, jQuery and JavaScript based on new app design.',
		client : 'Coconut Mobile Ltd',
		workfrom : 'Freelance work',
		tech : 'Adobe illustrator, Adobe Photoshop, InVision, HTML5, CSS3, jQuery, JavaScript',
		role : 'UX/UI designer and Front End Developer',
		type : 'Mobile App'
	},
	{
		id: '8',
		name : 'Sherwyn Williams',
		link : 'http://sherwynwilliams.co.nz/',
		img : 'img/sherwynwilliams-img.jpg',
		thumb : 'img/sherwynwilliams-thumb.jpg',
		description : 'Sherwyn Williams wanted to build a simple one page responsive website, I worked in web developer role, and build responsive website using static HTML5, CSS3 and jQuery. ',
		client : 'Sherwyn Williams',
		workfrom : 'Freelance work',
		tech : 'HTML5, CSS3,  jQuery',
		role : 'Web Developer',
		type : 'Web site'
	},
	{
		id: '9',
		name : 'The Fishing Website',
		link : 'http://www.fishing.net.nz/',
		img : 'img/fishing-site-img.jpg',
		thumb : 'img/fishing_site_thumb.jpg',
		description : 'The Fishing Website wanted upgrade responsive design with existing website that make mobile friendly. I worked in front end with a designer  and build responsiveness based on Mura CMS',
		client : 'The Fishing Website',
		workfrom : 'Freelance work',
		tech : 'HTML5, CSS3, jQuery, JavaScript, .NET, Adobe ColdFusion',
		role : 'Front End Developer',
		type : 'Web site'
	},
	{
		id: '10',
		name : 'Compass APP for Android',
		link : 'https://play.google.com/store/apps/details?id=nz.co.compass.billyhan',
		img : 'img/compass-app-img.jpg',
		thumb : 'img/compass-app-icon.jpg',
		description : 'Design and build Android app for personal project',
		client : '',
		workfrom : 'Self Teach',
		tech : 'PhoneGap / CORDOVA',
		role : 'Web Developer',
		type : 'Mobile App'
	}
];