var gulp = require('gulp'),
    gutil = require('gulp-util'),
    uglify = require('gulp-uglify'),
 	sass = require('gulp-sass'),
 	plumber = require('gulp-plumber'),
    livereload = require('gulp-livereload'),
 	prefix = require('gulp-autoprefixer'),
    webpack = require('gulp-webpack');
     
gulp.task('webpack', function() {
  return gulp.src('src/index.js')
    .pipe(webpack({
    	bail: false,
        debug: true,
	    watch: true,
		output: {
			filename: 'app.js'
		},
	    module: {
	      loaders: [
                { 
                    test: /.js?$/, 
                    loader: 'babel',
                    exclude: /node_modules|bower_components/, 
                    query: { presets: [ 'es2015' ] }
                },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loaders: ['react-hot-loader', 'babel-loader']
                },
                { 
                    test: /\.(png|woff|woff2|eot|ttf|svg)$/, 
                    loader: 'url-loader?limit=100000' },
                {
                    test: /\.css$/,
                    loader: 'style-loader!css-loader'
                }
            ],
	    },
	  }))
    .pipe(gulp.dest('dist/'));
});

// scss
gulp.task('styles',function(){
	//gulp.src('sass/*.scss')
	gulp.src('scss/style.scss')
		.pipe(sass({
			//style: 'compressed'
			//style: 'expend'
            outputStyle : 'compressed'
		}))
		.pipe(plumber())
		.pipe(prefix('> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1'))
		.pipe(gulp.dest('css/'))
		.pipe(livereload({ start: true }));
});

gulp.task('watch', function () {
    livereload.listen();
	gulp.watch(['src/*.js'], ['webpack']);
	gulp.watch('scss/*.scss', ['styles']);
});

gulp.task('default', ['webpack', 'styles', 'watch']);
//gulp.task('default', ['styles', 'watch']);